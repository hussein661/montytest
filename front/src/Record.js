import React, { Component } from "react";

class Record extends Component {
  state = {
    editMode: false,
    name: this.props.name,
    phone: this.props.phone,
    profession: this.props.profession,
    country:this.props.country,
    city:this.props.city,
    professions_id:this.props.professions_id,
    cities_id:this.props.cities_id,
    countries_id:this.props.countries_id,
    cities:[]
  };

  toggleEditMode = () => {
    const editMode = !this.state.editMode;
    this.setState({ editMode });
  };

  save = () => {
     const { id, updatePerson } = this.props;
    const { name, phone, profession, city } = this.state
    console.log(profession, city)
    updatePerson(id, { name, phone, profession, city });
    this.toggleEditMode();
  };

  startEditing = () => {
    this.setState({
      name: this.props.name,
      phone: this.props.phone,
      profession: this.props.profession,
      country:this.props.country,
      city:this.props.city,

    })
    this.toggleEditMode()

  }


  getAllCities = async e => {
    const response = await fetch(`http://localhost:8080/cities/${e}`);
    const answer = await response.json();
    if (answer) {
      const cities = answer.result;
      this.setState({ cities });
    }
  };

  handleCity = e => {
    this.setState({ country: e });
    this.getAllCities(e);
  };

  viewMode = () => (
    <tr>
      <td>{this.props.name}</td>
      <td>{this.props.phone}</td>
      <td>{this.props.profession}</td>
      <td>{this.props.country}</td>
      <td>{this.props.city}</td>
      <td>
        <button  className="buttonGreen" onClick={this.startEditing}>Edit</button>
      </td>
      <td>
        <button className="buttonRed" onClick={() => this.props.deletePerson(this.props.id)}>Delete</button>
      </td>
    </tr>
  );

  editMode = () => (
    <tr className="tableHeader">
      <td>
        <input type="text" name="name" defaultValue={this.props.name} onChange={(e)=>this.setState({name:e.target.value})} />
      </td>
      <td>
        <input type="text" name="phone" defaultValue={this.props.phone} onChange={(e)=>this.setState({phone:e.target.value})}/>
      </td>
      <td>
        <select name="professions" defaultValue={this.props.profession} onChange={(e)=>this.setState({profession:e.target.value})}>
          {this.props.professions.map(data => (
            <option value={data.id} key={data.id}>
              {data.name}
            </option>
          ))}
        </select>{" "}
      </td>
      <td>
        <select
          name="countries"
          defaultValue={this.props.country}
          onChange={e => this.handleCity(e.target.value)}
        >
          {this.props.countries.map(data => (
            <option value={data.id} key={data.id}>
              {data.name}
            </option>
          ))}
        </select>
      </td>
      <td>
        {" "}
        <select name="city" defaultValue={this.props.city} onChange={(e)=>this.setState({city:e.target.value})}>
          {this.state.cities.map(data => (
            <option value={data.id} key={data.id}>
              {data.name}
            </option>
          ))}
        </select>
      </td>
      <td colSpan="2">
        <button className="buttonGreen" onClick={this.save}>Save changes</button>
        <button className="buttonBlack" onClick={this.toggleEditMode}>Cancel</button>
      </td>
    </tr>
  );

  render() {
    if (this.state.editMode) {
      return this.editMode();
    }
    return this.viewMode();
  }
}

export default Record;
