import React from "react";
import "./styles.css";
import Record from "./Record";

class App extends React.Component {
  state = {
    all_people: [],
    countries: [],
    cities: [],
    professions: [],
    professions_id: null,
    cities_id: null,
    name: "",
    phone: "",
    profession: null,
    country: null,
    city: null,
  };



  getAllCountries = async () => {
    const response = await fetch(`http://localhost:8080/countries/all`);
    const answer = await response.json();
    if (answer) {
      const countries = answer.result;
      this.setState({ countries });
    }
  };

  getAllCities = async e => {
    const response = await fetch(`http://localhost:8080/cities/${e}`);
    const answer = await response.json();
    if (answer) {
      const cities = answer.result;
      this.setState({ cities });
    }
  };

  getAllProfessions = async () => {
    const response = await fetch(`http://localhost:8080/professions/all`);
    const answer = await response.json();
    if (answer) {
      const professions = answer.result;
      this.setState({ professions });
    }
  };

  getAllPeople = async () => {
    const response = await fetch(`http://localhost:8080/people/all`);
    const answer = await response.json();
    if (answer) {
      const all_people = answer.result;
      this.setState({ all_people });
    }
  };

  getPeopleBy = async (professions_id, cities_id) => {
    const response = await fetch(
      `http://localhost:8080/people/by?professions_id=${professions_id}&cities_id=${cities_id}`
    );
    const answer = await response.json();
    if (answer) {
      const all_people = answer.result;
      this.setState({ all_people });
    }
  };

  handleCities = e => {
    this.setState({ country: e });
    this.getAllCities(e);
  };
  componentDidMount() {
    this.getAllCountries();
    this.getAllProfessions();
  }

  showAll = () => {
    this.getAllPeople();
  };

  submit = e => {
    e.preventDefault();
    const { professions_id, cities_id } = this.state;
    this.getPeopleBy(professions_id, cities_id);
  };

  add = async () => {
    const { name, phone, profession, country, city } = this.state;
    const response = await fetch(
      `http://localhost:8080/people/add?name=${name}&phone=${phone}&professions_id=${profession}&countries_id=${country}&cities_id=${city}`
    );
    const answer = await response.json();
    if (answer) {
      const id = answer.result;
      const record = { name, phone, profession, country, city, id };
      const all_people = [...this.state.all_people, record];
      this.setState({ all_people });
      this.setState({
        name: "",
        phone: "",
        profession: "",
        country: "",
        city: ""
      });
    }
  };

  deletePerson = async id => {
    const response = await fetch(`http://localhost:8080/people/delete/${id}`);
    const answer = await response.json();
    if (answer) {
      const all_people = this.state.all_people.filter(
        person => person.id !== id
      );
      this.setState({ all_people });
    }
  };

  updatePerson = async (id, props) => {
    const response = await fetch(`http://localhost:8080/people/update/${id}?name=${props.name}&phone=${props.phone}&professions_id=${props.profession}&cities_id=${props.city}`)
    const answer = await response.json();
    if (answer) {
      const all_people = this.state.all_people.map(record => {
        if (record.id === id) {

          const updatedRecord = {
            id: record.id,
            name: props.name,
            phone: props.phone,
            professions_id: props.professions_id,
            countries_id: props.countries_id,
            cities_id:props.cities_id
          }
          return updatedRecord
        }
        else {
          return record
        }
      });
      this.setState({ all_people });
    }
  }

  render() {
    const all_people = this.state.all_people;
    return (
      <div className="App">
        <form onSubmit={this.submit}>
          <select
            name="profession"
            onChange={e => this.setState({ professions_id: e.target.value })}
          >
            {this.state.professions.map(data => (
              <option value={data.id} key={data.id}>
                {data.name}
              </option>
            ))}
          </select>
          <select
            name="countries"
            onChange={e => this.handleCities(e.target.value)}
          >
            {this.state.countries.map(data => (
              <option value={data.id} key={data.id}>
                {data.name}
              </option>
            ))}
          </select>
          <select
            name="cities"
            onChange={e => this.setState({ cities_id: e.target.value })}
          >
            {this.state.cities.map(data => (
              <option value={data.id} key={data.id}>
                {data.name}
              </option>
            ))}
          </select>

          <button type="submit" className="myButton">
            Search
          </button>
          <button type="button" onClick={this.showAll} className="myButton">
            Show all
          </button>
        </form>
        <table>
          <tr className="tableHeader">
            <td>name</td>
            <td>Phone Number</td>
            <td>Profession</td>
            <td>Country</td>
            <td>City</td>
            <td colSpan="2">Control</td>
          </tr>
          {all_people.length !== 0 ? (
            all_people.map(person => (
              <Record 
                key={person.id}
                id={person.id}
                name={person.name}
                phone={person.phone}
                profession={person.profession}
                country={person.country}
                city={person.city}
                professions_id={this.state.professions_id}
                countries_id={this.state.professions_id}
                cities_id={this.state.cities_id}
                deletePerson={this.deletePerson}
                updatePerson={this.updatePerson}
                countries={this.state.countries}
                cities={this.state.cities}
                professions={this.state.professions}
                handleCities={this.handleCities}
              />
            ))
          ) : (
            <p>no such columns</p>
          )}
          <tr className="tableHeader">
            <td>
              <input
                type="text"
                name="name"
                onChange={e => this.setState({ name: e.target.value })}
                value={this.state.name}
                required
              />
            </td>
            <td>
              <input
                type="text"
                name="phone"
                onChange={e => this.setState({ phone: e.target.value })}
                value={this.state.phone}
                required
              />
            </td>
            <td>
              <select
                name="profession"
                onChange={e => this.setState({ profession: e.target.value })}
                value={this.state.profession}
                required
              >
                {this.state.professions.map(data => (
                  <option value={data.id} key={data.id}>
                    {data.name}
                  </option>
                ))}
              </select>{" "}
            </td>
            <td>
              <select
                name="countries"
                onChange={e => this.handleCities(e.target.value)}
                value={this.state.country}
                required
              >
                {this.state.countries.map(data => (
                  <option value={data.id} key={data.id}>
                    {data.name}
                  </option>
                ))}
              </select>
            </td>
            <td>
              {" "}
              <select
                name="city"
                onChange={e => this.setState({ city: e.target.value })}
                value={this.state.city}
                required
              >
                {this.state.cities.map(data => (
                  <option value={data.id} key={data.id}>
                    {data.name}
                  </option>
                ))}
              </select>
            </td>
            <td colSpan="2">
              <button className="buttonGreen" onClick={this.add}>Add +</button>
            </td>
          </tr>
        </table>
      </div>
    );
  }
}

export default App;
