import sqlite from 'sqlite'
import SQL from 'sql-template-strings';



const initializeDatabase = async () => {
  const db = await sqlite.open('./db.sqlite');

  const getAllPeople = async () => {
      
    const people = await db.all(SQL `select people.id,people.name,people.phone,countries.country,cities.city,professions.profession from people,cities,professions,countries where
    cities.id = people.cities_id and
    professions.id = people.professions_id and
    countries.id = cities.countries_id
      `);
    if(!people){
      return ("no rows exists")
    }
    return people;
  }

  const getAllBy = async (professions_id,cities_id) => {

    const people = await db.all(SQL `select people.id,people.name,people.phone,countries.country,cities.city,professions.profession from people,cities,professions,countries where
    cities.id = people.cities_id and
    professions.id = people.professions_id and
    countries.id = cities.countries_id and
    cities.id = ${cities_id} and
    professions.id = ${professions_id}
  `);
    if(!people){
      return ("no rows exists")
    }
    return people;
  }


  const addPerson = async (props) => {
    const {name,phone,professions_id,cities_id} = props;
    const addnew = await db.run(SQL`insert into people (name,phone,professions_id,cities_id) values (${name},${phone},${professions_id},${cities_id})`)
    return addnew
  }

  const deletePerson = async (id) => {
    const deletePerson = await db.run(SQL`delete from people where rowid = ${id}`)
    if(deletePerson.stmt.changes === 0){
      return(`record of id : "${id}" does not exist`)
    }
    return ("deleted successfully") 
  }

  const updatePerson = async (id,name,phone,professions_id,cities_id) => {
    console.log(id,name,phone,professions_id,cities_id);
    const updatePerson = await db.run(SQL`update people set name=${name},phone=${phone},professions_id=${professions_id},cities_id=${cities_id} where rowid = ${id}`)
    return updatePerson
  }

  const getAllCities = async (e) => {
      
    const rows = await db.all(SQL `select id,city as name from cities where countries_id = ${e}order by id desc`);
    if(!rows){
      return ("no rows exists")
    }
    return rows;
  }



  const getAllCountries = async () => {
      
    const rows = await db.all(SQL `select  id,country as name from countries order by id desc`);
    if(!rows){
      return ("no rows exists")
    }
    return rows;
  }

  const getAllProfessions = async () => {
      
    const rows = await db.all(SQL `select id,profession as name from professions order by id desc`);
    if(!rows){
      return ("no rows exists")
    }
    return rows;
  }

  const controller = {
    getAllPeople,
    getAllCities,
    getAllCountries,
    getAllProfessions,
    addPerson,
    deletePerson,
    updatePerson,
    getAllBy
  }
  return controller
  

  }
  
  
  export default initializeDatabase;
  