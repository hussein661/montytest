import app from './app'
import initializeDatabase from './db'


const start = async () => { 
  
  const controller = await initializeDatabase();


  app.get('/people/all', async (req, res, next) => {
    const people = await controller.getAllPeople()
    res.json({success:true, result:people})
  })

  app.get('/people/add',async(req,res)=>{
    const {name,phone,professions_id,cities_id} = req.query
    const addPerson = await controller.addPerson({name,phone,professions_id,cities_id})
    res.json({success:true,result:addPerson})

  })

  app.get('/people/delete/:id',async(req,res) => {
    const {id} = req.params
    const deletePerson = await controller.deletePerson(id)
    res.json({success:true,result:deletePerson})

  })

  app.get('/people/update/:id',async(req,res)=> {
    const {id} = req.params
    const {name,phone,professions_id,cities_id} = req.query
    console.log(name,phone,professions_id,cities_id)
    const updatePerson = await controller.updatePerson(id,name,phone,professions_id,cities_id)
    res.json({success:true,result:updatePerson})
  })

  app.get('/cities/:e', async (req, res, next) => {
    const {e} = req.params
    const cities = await controller.getAllCities(e)
    res.json({success:true, result:cities})
  })

  app.get('/professions/all', async (req, res, next) => {
    const professions = await controller.getAllProfessions()
    res.json({success:true, result:professions})
  })

  app.get('/countries/all', async (req, res, next) => {
    const countries = await controller.getAllCountries()
    res.json({success:true, result:countries})
  })


  app.get('/people/by', async (req, res, next) => {
    const {professions_id,cities_id} = req.query
    const people= await controller.getAllBy(professions_id,cities_id)
    res.json({success:true, result:people})
  })

}

start()

app.listen( 8080, () => console.log('server listening on port 8080') )
